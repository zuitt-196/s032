let http = require("http");
//mock collection of courses:

    let courses =[
        {
            name:"Python 101",
            description:"Learn Python",
            price:25000
        },
        {
            name:"React 101",
            description:"Learn React",
            price:35000
        },
        {
            name:"ExpressJS 101",
            description:"Learn ExpressJS",
            price:28000
        }

    ]
http.createServer(function(request,response){

    console.log(request.url); 
    console.log(request.method);
    if(request.url === "/" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Hello from our 4th server. This is the / endpoint");

    }else if (request.url ==="/" && request.method ==="POST"){
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Thiw is the / endpoint. POST method request");

    }else if (request.url ==="/" && request.method ==="PUT") {
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Thiw is the / endpoint. PUT method request");

} else if (request.url ==="/" && request.method ==="DELETE") {
    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end("Thiw is the / endpoint. DELETE method request");

}else if (request.url ==="/courses" && request.method ==="GET") {
    response.writeHead(200,{'Content-Type':'application/json'});
    response.end(JSON.stringify(courses));
    //stringinfy our courses array to send it as a

}
else if (request.url ==="/courses" && request.method ==="POST") {
        

   // making variable 
    let requestBody =  "";

    request.on('data',function(data){
        // console.log(data);

        requestBody += data;
    })


    request.on('end',function(){

        
        // console.log(requestBody);



        // Initially , requestBody in JSON format


        requestBody = JSON.parse(requestBody);
        // console.log(requestBody); //requestBody is now an object


        // add then requestBody in the array 
        courses.push(requestBody);

        //check the course array if we were able to add our requestBody
        // console.log(courses);
        response.writeHead(200,{'Content-Type':'application/json'});

        // send the  updated course array to the client as JSON array
        response.end(JSON.stringify(courses));
    
    })

    // end step - this will run once or after the request data has been completely sent from the client

    // response.writeHead(200,{'Content-Type':'text/plain'});
    // response.end("This is a response to a POST method request for the /course ");

}


}).listen(4000);

console.log("Server running at localhost:4000");