// create simple server and the following routes thier correspondind HTTP methods response 


let http = require("http");


http.createServer(function(request,response) {
        //  console.log(request.url); 
        //  console.log(request.method);
        if (request.url === "/" &&  request.method === "GET") {
            
            response.writeHead(200,{'Content-type':'text/plain'});
            response.end("Welcome to Booking System");
        }else if (request.url ==="/profile" && request.method === "POST") {
            response.writeHead(200,{'Content-type':'text/plain'});
             response.end("Welcome to your profile");  
        }else if (request.url ==="/course" && request.method === "GET") {
            response.writeHead(200,{'Content-type':'text/plain'});
             response.end("Here's our courses available ");  
        }else if (request.url ==="/addcourse" && request.method === "POST") {
            response.writeHead(200,{'Content-type':'text/plain'});
             response.end("Add a course to oue resources");  
        }else if (request.url ==="/updatecourse" && request.method === "PUT") {
            response.writeHead(200,{'Content-type':'text/plain'});
             response.end("Update a course to our resources");  
        }else if (request.url ==="/archivecourse" && request.method === "DELETE") {
            response.writeHead(200,{'Content-type':'text/plain'});
             response.end("Archive course to oue resources");  
        }


    
}).listen(5000)


console.log("Server running at localhost:5000!");